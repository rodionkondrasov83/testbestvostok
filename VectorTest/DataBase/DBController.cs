﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VectorTest
{
    static class DBController
    {
        public static string ConnectionString = ConfigurationManager.ConnectionStrings["Connection"].ConnectionString;
        static DBController() 
        {
        }

        ///<summary>
        ///Async execute sql query, return DataTable
        ///</summary>
        public async static Task<DataTable> AsyncExecuteSqlQuery(string sqlCommand)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                var sqlDataAdapter = new SqlDataAdapter(sqlCommand, connection);
                var reader = await  sqlDataAdapter.SelectCommand.ExecuteReaderAsync();
                DataTable dataTable = new DataTable();
                dataTable.Load(reader);

                connection.Close();
                return dataTable;
            }
        }

        public async static void AsyncNonExecuteSqlQuery(string sqlCommand)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                var cmd = connection.CreateCommand();
                cmd.CommandText = sqlCommand;

                try
                {
                    await cmd.ExecuteNonQueryAsync();
                }
                catch 
                {
                    //в этом месте должно быть логирование
                    throw;
                }
                
                connection.Close();
            }
        }

        ///<summary>
        ///Async execute sql procedure, return DataTable
        ///Array of parameters = names of parameters in procedure
        ///</summary>
        public async static Task<DataTable> AsyncExecuteSqlProcedure(string procedureName, IEnumerable<(string, string)> parameters = null)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                var cmd = new SqlCommand(procedureName, connection);
                cmd.CommandType = CommandType.StoredProcedure;

                //заполнение параметров
                if (parameters != null)
                {
                    foreach ((string, string) param in parameters)
                    {
                        SqlParameter newParam = new SqlParameter
                        {
                            ParameterName = "@" + param,
                            Value = param
                        };
                        cmd.Parameters.Add(newParam);
                    }
                }

                var sqlDataAdapter = new SqlDataAdapter(cmd);
                var reader = await sqlDataAdapter.SelectCommand.ExecuteReaderAsync();
                DataTable dataTable = new DataTable();
                dataTable.Load(reader);

                connection.Close();
                return dataTable;
            }
        }

        public static DataTable ExecuteSqlProcedure(string procedureName, IEnumerable<(string, string)> parameters = null)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                var cmd = new SqlCommand(procedureName, connection);
                cmd.CommandType = CommandType.StoredProcedure;

                //заполнение параметров
                if (parameters != null)
                {
                    foreach ((string, string) param in parameters)
                    {
                        SqlParameter newParam = new SqlParameter
                        {
                            ParameterName = "@" + param,
                            Value = param
                        };
                        cmd.Parameters.Add(newParam);
                    }
                }

                var sqlDataAdapter = new SqlDataAdapter(cmd);
                var reader = sqlDataAdapter.SelectCommand.ExecuteReader();
                DataTable dataTable = new DataTable();
                dataTable.Load(reader);

                connection.Close();
                return dataTable;
            }
        }

        public async static void AsyncNonExecuteSqlProcedure(string procedureName, IEnumerable<(string, string)> parameters = null)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                var cmd = new SqlCommand(procedureName, connection);
                cmd.CommandType = CommandType.StoredProcedure;

                //заполнение параметров
                if (parameters != null)
                {
                    foreach ((string, string) param in parameters)
                    {
                        SqlParameter newParam = new SqlParameter
                        {
                            ParameterName = "@" + param.Item1,
                            Value = param.Item2
                        };
                        cmd.Parameters.Add(newParam);
                    }
                }

                try
                {
                    await cmd.ExecuteNonQueryAsync();
                }
                catch
                {
                    //в этом месте должно быть логирование
                    throw;
                }

                connection.Close();
            }
        }
    }
}

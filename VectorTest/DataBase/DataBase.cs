﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VectorTest
{
    public class DataBase
    {
        private static DataBase _instance;

        public event EventHandler DataBaseUpdated;

        public DataTable Orders;
        public DataTable Status;
        public DataTable Cars;
        public DataTable Persons;
        public DataTable Jobs;
        private DataBase()
        {
            InitializeDB();
            UpdateDB();
        }

        private void InitializeDB()
        {
            Orders = new DataTable();
            Status = new DataTable();
            Cars = new DataTable();
            Persons = new DataTable();
            Jobs = new DataTable();
        }

        private async void FillDB()
        {
            Orders = await DBController.AsyncExecuteSqlProcedure("PR_ORDER_SELECT");
            Status =  await DBController.AsyncExecuteSqlProcedure("PR_STATUS_SELECT");
            Cars =  await DBController.AsyncExecuteSqlProcedure("PR_CAR_SELECT");
            Persons =  await DBController.AsyncExecuteSqlProcedure("PR_PERSON_SELECT");
            Jobs =  await DBController.AsyncExecuteSqlProcedure("PR_JOBS_SELECT");
        }

        private void ClearDB() 
        {
            Orders = new DataTable();
            Status = new DataTable();
            Cars = new DataTable();
            Persons = new DataTable();
            Jobs = new DataTable();
        }

        public void UpdateDB()
        {
            ClearDB();
            FillDB();
           DataBaseUpdated?.Invoke(null, EventArgs.Empty);
        }

        public async void CreateNewPerson(string namePerson)
        {
            var paramets = new List<(string, string)> { ("Name", namePerson) };
            DBController.AsyncNonExecuteSqlProcedure("PR_PERSONS_ADDNEWPERSONS", paramets);

            Persons = await DBController.AsyncExecuteSqlProcedure("PR_PERSON_SELECT");
            DataBaseUpdated?.Invoke(null, EventArgs.Empty);
        }

        public async void CreateNewCar(int idPerson, string brand, string model)
        {
            var paramets = new List<(string, string)>();
            paramets.Add(("IDPerson", idPerson.ToString()));
            paramets.Add(("Brand", brand.ToString()));
            paramets.Add(("Model", model.ToString()));
            DBController.AsyncNonExecuteSqlProcedure("PR_CAR_ADDNEWCAR", paramets);

            Cars = await DBController.AsyncExecuteSqlProcedure("PR_CAR_SELECT");
            DataBaseUpdated?.Invoke(null, EventArgs.Empty);
        }

        public async void CreateNewOrder(int idPerson, int idCar, string braeking)
        {
            var paramets = new List<(string, string)>();
            paramets.Add(("IDPerson", idPerson.ToString()));
            paramets.Add(("IDCar", idCar.ToString()));
            paramets.Add(("Breaking", braeking));

            DBController.AsyncNonExecuteSqlProcedure("PR_ORDER_ADDNEWORDER", paramets);

            Orders  = await DBController.AsyncExecuteSqlProcedure("PR_ORDER_SELECT");
            DataBaseUpdated?.Invoke(null, EventArgs.Empty);
        }

        public async void SetStartOrder(int id, DateTime startTime)
        {
            var paramets = new List<(string, string)>();
            paramets.Add(("ID", id.ToString()));
            paramets.Add(("StartTime", startTime.ToString()));

            DBController.AsyncNonExecuteSqlProcedure("PR_ORDER_SETSTARTTIME", paramets);

            Orders = await DBController.AsyncExecuteSqlProcedure("PR_ORDER_SELECT");
            DataBaseUpdated?.Invoke(null, EventArgs.Empty);
        }

        public async void SetEndOrder(int id, DateTime endTime)
        {
            var paramets = new List<(string, string)>();
            paramets.Add(("ID", id.ToString()));
            paramets.Add(("EndTime", endTime.ToString()));

            DBController.AsyncNonExecuteSqlProcedure("PR_ORDER_SETENDTIME", paramets);

            Orders = await DBController.AsyncExecuteSqlProcedure("PR_ORDER_SELECT");
            DataBaseUpdated?.Invoke(null, EventArgs.Empty);
        }

        public async void CreateNewJob(int idOrder, int idMaster, string job, DateTime startTime, DateTime endTime)
        {
            var paramets = new List<(string, string)>();
            paramets.Add(("IDOrders", idOrder.ToString()));
            paramets.Add(("IDMaster", idMaster.ToString()));
            paramets.Add(("Jobs", job));
            paramets.Add(("StartTime", startTime.ToString()));
            paramets.Add(("EndTime", endTime.ToString()));

            DBController.AsyncNonExecuteSqlProcedure("PR_JOBS_ADDNEWJOBS", paramets);

            Jobs = await DBController.AsyncExecuteSqlProcedure("PR_JOBS_SELECT");
            DataBaseUpdated?.Invoke(null, EventArgs.Empty);
        }

        public static DataBase GetInstance()
        {
            if (_instance == null)
                _instance = new DataBase();
            return _instance;
        }
    }
}

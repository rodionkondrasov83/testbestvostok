﻿namespace VectorTest
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.NewOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.NewOrderButton = new System.Windows.Forms.Button();
            this.CloseOrder = new System.Windows.Forms.Button();
            this.OrdersDataGridView = new System.Windows.Forms.DataGridView();
            this.NewJobs = new System.Windows.Forms.Button();
            this.OrderCardForm = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OrdersDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NewOrderToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(144, 26);
            // 
            // NewOrderToolStripMenuItem
            // 
            this.NewOrderToolStripMenuItem.Name = "NewOrderToolStripMenuItem";
            this.NewOrderToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.NewOrderToolStripMenuItem.Text = "Новый заказ";
            // 
            // NewOrderButton
            // 
            this.NewOrderButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.NewOrderButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NewOrderButton.Location = new System.Drawing.Point(899, 12);
            this.NewOrderButton.Name = "NewOrderButton";
            this.NewOrderButton.Size = new System.Drawing.Size(164, 82);
            this.NewOrderButton.TabIndex = 1;
            this.NewOrderButton.Text = "Новый заказ";
            this.NewOrderButton.UseVisualStyleBackColor = true;
            this.NewOrderButton.Click += new System.EventHandler(this.NewOrderButton_Click);
            // 
            // CloseOrder
            // 
            this.CloseOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CloseOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CloseOrder.Location = new System.Drawing.Point(899, 364);
            this.CloseOrder.Name = "CloseOrder";
            this.CloseOrder.Size = new System.Drawing.Size(164, 82);
            this.CloseOrder.TabIndex = 2;
            this.CloseOrder.Text = "Закрыть заказ";
            this.CloseOrder.UseVisualStyleBackColor = true;
            this.CloseOrder.Click += new System.EventHandler(this.CloseOrder_Click);
            // 
            // OrdersDataGridView
            // 
            this.OrdersDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.OrdersDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.OrdersDataGridView.Location = new System.Drawing.Point(-1, 0);
            this.OrdersDataGridView.Name = "OrdersDataGridView";
            this.OrdersDataGridView.Size = new System.Drawing.Size(894, 454);
            this.OrdersDataGridView.TabIndex = 3;
            // 
            // NewJobs
            // 
            this.NewJobs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.NewJobs.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NewJobs.Location = new System.Drawing.Point(899, 188);
            this.NewJobs.Name = "NewJobs";
            this.NewJobs.Size = new System.Drawing.Size(164, 82);
            this.NewJobs.TabIndex = 4;
            this.NewJobs.Text = "Добавить проведенные работы";
            this.NewJobs.UseVisualStyleBackColor = true;
            this.NewJobs.Click += new System.EventHandler(this.NewJobs_Click);
            // 
            // OrderCardForm
            // 
            this.OrderCardForm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.OrderCardForm.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.OrderCardForm.Location = new System.Drawing.Point(899, 100);
            this.OrderCardForm.Name = "OrderCardForm";
            this.OrderCardForm.Size = new System.Drawing.Size(164, 82);
            this.OrderCardForm.TabIndex = 5;
            this.OrderCardForm.Text = "Карточка заказа";
            this.OrderCardForm.UseVisualStyleBackColor = true;
            this.OrderCardForm.Click += new System.EventHandler(this.OrderCardForm_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button2.Location = new System.Drawing.Point(899, 276);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(164, 82);
            this.button2.TabIndex = 6;
            this.button2.Text = "Приостановить выполнение";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1075, 451);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.OrderCardForm);
            this.Controls.Add(this.NewJobs);
            this.Controls.Add(this.OrdersDataGridView);
            this.Controls.Add(this.CloseOrder);
            this.Controls.Add(this.NewOrderButton);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.OrdersDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem NewOrderToolStripMenuItem;
        private System.Windows.Forms.Button NewOrderButton;
        private System.Windows.Forms.Button CloseOrder;
        private System.Windows.Forms.DataGridView OrdersDataGridView;
        private System.Windows.Forms.Button NewJobs;
        private System.Windows.Forms.Button OrderCardForm;
        private System.Windows.Forms.Button button2;
    }
}


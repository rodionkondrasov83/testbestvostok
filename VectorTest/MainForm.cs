﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VectorTest
{
    public partial class MainForm : Form
    {
        public DataBase _dB = DataBase.GetInstance();
        public MainForm()
        {
            InitializeComponent();
            ConstructOrderDataGridTable();

            _dB.DataBaseUpdated += (sender, e) => { OrdersDataGridView.Refresh(); };
        }

        private void NewOrderButton_Click(object sender, EventArgs e)
        {
            CreateOrderForm createOrderForm = new CreateOrderForm();
            createOrderForm.ShowDialog();
        }

        private void NewJobs_Click(object sender, EventArgs e)
        {
            if (!IsSelectedOrder()) return;
            CreateJobsForm createJobsForm = new CreateJobsForm((int)OrdersDataGridView.SelectedRows[0].Cells[0].Value);
            createJobsForm.ShowDialog();

        }

        private bool IsSelectedOrder()
        {
            if (OrdersDataGridView.SelectedRows.Count != 1)
            {
                DialogForm errorForm = new DialogForm("Пожалуйста, выберите один заказ в таблице");
                errorForm.ShowDialog();
                return false;
            }
            return true;
        }

        private void UpdateOrderGridView()
        {
            ConstructOrderDataGridTable();
        }

        private void ConstructOrderDataGridTable()
        {
            OrdersDataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            OrdersDataGridView.AllowUserToAddRows = false;
            OrdersDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            OrdersDataGridView.DataSource = _dB.Orders;
            OrdersDataGridView.Columns["ID"].ReadOnly = true;
            OrdersDataGridView.Columns["ID"].HeaderText = "Номер";
            OrdersDataGridView.Columns["Name"].HeaderText = "Владелец";
            OrdersDataGridView.Columns["Car"].HeaderText = "Автомобиль";
            OrdersDataGridView.Columns["NameStatus"].HeaderText = "Статус";
            OrdersDataGridView.Columns["RegDate"].HeaderText = "Регистрация";
            OrdersDataGridView.Columns["StartDate"].HeaderText = "Начало работ";
            OrdersDataGridView.Columns["EndDate"].HeaderText = "Закрытие";

            OrdersDataGridView.Refresh();
        }

        private void CloseOrder_Click(object sender, EventArgs e)
        {
            _dB.SetEndOrder((int)OrdersDataGridView.SelectedRows[0].Cells[0].Value, DateTime.Now);
            OrderViewForm orderViewForm = new OrderViewForm((int)OrdersDataGridView.SelectedRows[0].Cells[0].Value);
            orderViewForm.ShowDialog();
        }

        private void OrderCardForm_Click(object sender, EventArgs e)
        {
            OrderViewForm orderViewForm = new OrderViewForm((int)OrdersDataGridView.SelectedRows[0].Cells[0].Value);
            orderViewForm.ShowDialog();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VectorTest
{
    public class IDLabelPair
    {
        private int _id;
        private string _text;

        public IDLabelPair(int id, string text)
        {

            this._id = id;
            this._text = text;
        }

        public int ID
        {
            get
            {
                return _id;
            }
        }

        public string Text
        {

            get
            {
                return _text;
            }
        }
    }
}

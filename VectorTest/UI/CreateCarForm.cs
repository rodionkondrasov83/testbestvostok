﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VectorTest
{
    public partial class CreateCarForm : Form
    {
        DataBase _dB = DataBase.GetInstance();
        public CreateCarForm()
        {
            InitializeComponent();
            UpdatePersonComboBox();

            PersonComboBox.ValueMember = "ID";
            PersonComboBox.DisplayMember = "Text";
        }
        private void UpdatePersonComboBox()
        {
            PersonComboBox.Items.Clear();

            var personList = new List<IDLabelPair>();

            foreach (DataRow row in _dB.Persons.Rows)
            {
                personList.Add(new IDLabelPair((int)row[0], row[1].ToString()));
            }

            PersonComboBox.DataSource = personList;

            if (PersonComboBox.Items.Count != 0)
            {
                PersonComboBox.SelectedIndex = 0;
            }
        }

        private void RegButton_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(PersonComboBox.Text) 
                && String.IsNullOrEmpty(ModelTextBox.Text) 
                && String.IsNullOrEmpty(BrandTextBox.Text))
            {
                DialogForm errorForm = new DialogForm("Пожалуйста, заполните все поля");
                return;
            }

            _dB.CreateNewCar((int)PersonComboBox.SelectedValue, BrandTextBox.Text, ModelTextBox.Text);

            DialogForm dialogForm = new DialogForm("Новый автомобиль добавлен");
            if (dialogForm.ShowDialog() != null) Close();

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VectorTest
{
    public partial class CreateJobsForm : Form
    {
        DataBase _dB = DataBase.GetInstance();

        private int _selectedIDOrder;
        public CreateJobsForm(int selectedIDOrder)
        {
            InitializeComponent();

            _selectedIDOrder = selectedIDOrder;
        }

        private void RegButton_Click(object sender, EventArgs e)
        {
            _dB.CreateNewJob(_selectedIDOrder, 0, JobTextBox.Text, startTimePicker.Value, endTimePicker.Value);

            var db = _dB.Orders;
            DataRow row = _dB.Orders.Select($"ID = {_selectedIDOrder}")[0];

            if (String.IsNullOrEmpty(row["StartDate"].ToString()))
                _dB.SetStartOrder((int)row["ID"], startTimePicker.Value);

            DialogForm dialogForm = new DialogForm("Работы успешно зафиксированны");
            if (dialogForm.ShowDialog() != null) Close();
        }
    }
}

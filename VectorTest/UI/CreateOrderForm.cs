﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VectorTest
{
    public partial class CreateOrderForm : Form
    {
        DataBase _dB = DataBase.GetInstance();
        public CreateOrderForm()
        {
            InitializeComponent();

            CarComboBox.ValueMember = "ID";
            CarComboBox.DisplayMember = "Text";

            PersonComboBox.ValueMember = "ID";
            PersonComboBox.DisplayMember = "Text";

            FillComponents();

            _dB.DataBaseUpdated += (sender, e) => {
                CarComboBox.Refresh();
                PersonComboBox.Refresh();
            };

        }

        private void FillComponents()
        {
            UpdatePersonComboBox();
            UpdateCarComboBox();
        }

        private void UpdatePersonComboBox()
        {
            var personList = new List<IDLabelPair>();

            foreach (DataRow row in _dB.Persons.Rows)
            {
                personList.Add(new IDLabelPair((int)row[0], row[1].ToString()));
            }

            PersonComboBox.DataSource = personList;

            if (PersonComboBox.Items.Count != 0)
            {
                PersonComboBox.SelectedIndex = 0;
            }
        }

        private void UpdateCarComboBox()
        {
            var carList = new List<IDLabelPair>();
            foreach (DataRow row in _dB.Cars.Rows)
            {
                carList.Add(new IDLabelPair((int)row[0],row[2].ToString() + " " + row[3].ToString()));
            }

            CarComboBox.DataSource = carList;

            if (CarComboBox.Items.Count != 0)
            {
                CarComboBox.SelectedIndex = 0;
            }
        }

        private void NewCarButton_Click(object sender, EventArgs e)
        {
            CreateCarForm carForm = new CreateCarForm();
            carForm.ShowDialog();
        }

        private void NewClientButton_Click(object sender, EventArgs e)
        {
            CreatePerson personForm = new CreatePerson();
            personForm.ShowDialog();
        }

        private void RegButton_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(PersonComboBox.Text)
                && String.IsNullOrEmpty(CarComboBox.Text))
            {
                DialogForm errorForm = new DialogForm("Пожалуйста, заполните все поля");
                return;
            }

            _dB.CreateNewOrder((int)PersonComboBox.SelectedValue, (int)CarComboBox.SelectedValue, BrekingTextBox.Text);

            DialogForm dialogForm = new DialogForm("Заказ успешно зарегистрирован");
            dialogForm.ShowDialog();

        }
    }
}

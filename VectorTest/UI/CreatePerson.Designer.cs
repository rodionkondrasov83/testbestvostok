﻿namespace VectorTest
{
    partial class CreatePerson
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PersonNameLabel = new System.Windows.Forms.Label();
            this.PersonNameTexBox = new System.Windows.Forms.TextBox();
            this.RegButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // PersonNameLabel
            // 
            this.PersonNameLabel.AutoSize = true;
            this.PersonNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PersonNameLabel.Location = new System.Drawing.Point(12, 21);
            this.PersonNameLabel.Name = "PersonNameLabel";
            this.PersonNameLabel.Size = new System.Drawing.Size(156, 24);
            this.PersonNameLabel.TabIndex = 5;
            this.PersonNameLabel.Text = "ФИО Владельца";
            // 
            // PersonNameTexBox
            // 
            this.PersonNameTexBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PersonNameTexBox.Location = new System.Drawing.Point(174, 21);
            this.PersonNameTexBox.Name = "PersonNameTexBox";
            this.PersonNameTexBox.Size = new System.Drawing.Size(264, 29);
            this.PersonNameTexBox.TabIndex = 6;
            // 
            // RegButton
            // 
            this.RegButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RegButton.Location = new System.Drawing.Point(444, 12);
            this.RegButton.Name = "RegButton";
            this.RegButton.Size = new System.Drawing.Size(175, 51);
            this.RegButton.TabIndex = 10;
            this.RegButton.Text = "Зарегистрировать";
            this.RegButton.UseVisualStyleBackColor = true;
            this.RegButton.Click += new System.EventHandler(this.RegButton_Click);
            // 
            // CreatePerson
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(635, 75);
            this.Controls.Add(this.RegButton);
            this.Controls.Add(this.PersonNameTexBox);
            this.Controls.Add(this.PersonNameLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "CreatePerson";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CreatePerson";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label PersonNameLabel;
        private System.Windows.Forms.TextBox PersonNameTexBox;
        private System.Windows.Forms.Button RegButton;
    }
}
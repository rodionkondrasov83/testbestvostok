﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VectorTest
{
    public partial class CreatePerson : Form
    {
        DataBase _dB = DataBase.GetInstance();
        public CreatePerson()
        {
            InitializeComponent();
        }

        private void RegButton_Click(object sender, EventArgs e)
        {
            if(String.IsNullOrEmpty(PersonNameTexBox.Text))
            {
                //Здесь выводится диалоговое окно об ошибочном заполнении
                return;
            }
            _dB.CreateNewPerson(PersonNameTexBox.Text);

            DialogForm dialogForm = new DialogForm("Новый пользователь добавлен");
            if (dialogForm.ShowDialog() != null) Close();
            
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VectorTest
{
    public partial class DialogForm : Form
    {
        public DialogForm(string text)
        {
            InitializeComponent();
            DialogLabel.Text = text;
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}

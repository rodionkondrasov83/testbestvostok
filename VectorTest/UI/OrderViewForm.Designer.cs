﻿namespace VectorTest
{
    partial class OrderViewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.carStationDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.carStationDataSet = new VectorTest.Reports.CarStationDataSet();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.pRREPORTORDERSELECTHEADERBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pR_REPORT_ORDER_SELECT_HEADERTableAdapter = new VectorTest.Reports.CarStationDataSetTableAdapters.PR_REPORT_ORDER_SELECT_HEADERTableAdapter();
            this.pRREPORTORDERSELECTBODYBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pR_REPORT_ORDER_SELECTBODYTableAdapter = new VectorTest.Reports.CarStationDataSetTableAdapters.PR_REPORT_ORDER_SELECTBODYTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.carStationDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.carStationDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRREPORTORDERSELECTHEADERBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRREPORTORDERSELECTBODYBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // carStationDataSetBindingSource
            // 
            this.carStationDataSetBindingSource.DataSource = this.carStationDataSet;
            this.carStationDataSetBindingSource.Position = 0;
            // 
            // carStationDataSet
            // 
            this.carStationDataSet.DataSetName = "CarStationDataSet";
            this.carStationDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "Header";
            reportDataSource1.Value = this.pRREPORTORDERSELECTHEADERBindingSource;
            reportDataSource2.Name = "Body";
            reportDataSource2.Value = this.pRREPORTORDERSELECTBODYBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource2);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "VectorTest.Reports.ReportCard.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ServerReport.BearerToken = null;
            this.reportViewer1.Size = new System.Drawing.Size(1292, 450);
            this.reportViewer1.TabIndex = 0;
            // 
            // pRREPORTORDERSELECTHEADERBindingSource
            // 
            this.pRREPORTORDERSELECTHEADERBindingSource.DataMember = "PR_REPORT_ORDER_SELECT_HEADER";
            this.pRREPORTORDERSELECTHEADERBindingSource.DataSource = this.carStationDataSetBindingSource;
            // 
            // pR_REPORT_ORDER_SELECT_HEADERTableAdapter
            // 
            this.pR_REPORT_ORDER_SELECT_HEADERTableAdapter.ClearBeforeFill = true;
            // 
            // pRREPORTORDERSELECTBODYBindingSource
            // 
            this.pRREPORTORDERSELECTBODYBindingSource.DataMember = "PR_REPORT_ORDER_SELECTBODY";
            this.pRREPORTORDERSELECTBODYBindingSource.DataSource = this.carStationDataSetBindingSource;
            // 
            // pR_REPORT_ORDER_SELECTBODYTableAdapter
            // 
            this.pR_REPORT_ORDER_SELECTBODYTableAdapter.ClearBeforeFill = true;
            // 
            // OrderViewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1292, 450);
            this.Controls.Add(this.reportViewer1);
            this.Name = "OrderViewForm";
            this.Text = "OrderViewForm";
            this.Load += new System.EventHandler(this.OrderViewForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.carStationDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.carStationDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRREPORTORDERSELECTHEADERBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRREPORTORDERSELECTBODYBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource carStationDataSetBindingSource;
        private Reports.CarStationDataSet carStationDataSet;
        private System.Windows.Forms.BindingSource pRREPORTORDERSELECTHEADERBindingSource;
        private System.Windows.Forms.BindingSource pRREPORTORDERSELECTBODYBindingSource;
        private Reports.CarStationDataSetTableAdapters.PR_REPORT_ORDER_SELECT_HEADERTableAdapter pR_REPORT_ORDER_SELECT_HEADERTableAdapter;
        private Reports.CarStationDataSetTableAdapters.PR_REPORT_ORDER_SELECTBODYTableAdapter pR_REPORT_ORDER_SELECTBODYTableAdapter;
    }
}
﻿
using Microsoft.Reporting.WinForms;
using System;
using System.Configuration;
using System.Windows.Forms;

namespace VectorTest
{
    public partial class OrderViewForm : Form
    {
        private int _idOrder;

        public OrderViewForm(int idOrder)
        {
            InitializeComponent();
            _idOrder = idOrder;
        }

        private void OrderViewForm_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "carStationDataSet.PR_REPORT_ORDER_SELECT_HEADER". При необходимости она может быть перемещена или удалена.
            this.pR_REPORT_ORDER_SELECT_HEADERTableAdapter.Fill(this.carStationDataSet.PR_REPORT_ORDER_SELECT_HEADER,_idOrder);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "carStationDataSet.PR_REPORT_ORDER_SELECTBODY". При необходимости она может быть перемещена или удалена.
            this.pR_REPORT_ORDER_SELECTBODYTableAdapter.Fill(this.carStationDataSet.PR_REPORT_ORDER_SELECTBODY,_idOrder);
            reportViewer1.LocalReport.SetParameters(new ReportParameter("IDOrders", _idOrder.ToString()));
            this.reportViewer1.RefreshReport();
        }
    }
}
